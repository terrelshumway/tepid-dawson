(function() {
    "use strict";
    var alarm;

    function listen(target, event, callback) {
        if (typeof target === 'string') {
            target = document.querySelector(target);
        }
        target.addEventListener(event, callback);
        return target;
    }

    function Alarm() {
        var that, media, curId, isMuted=false, curVolume=0.25;
        that = {
            select: function(soundid) {
                if (curId === soundid) {
                    return;
                } else {
                    that.stop();
                }
                media = document.querySelector("#" + soundid + " audio");
                if (media) {
                    curId = soundid;
                    media.volume = curVolume;
                    media.muted = isMuted;
                }
            },
            play: function(soundid) {
                if (typeof soundid === 'undefined') {
                    soundid = curId;
                } else {
                    that.select(soundid);
                }
                media.play();
            },
            stop: function() {
                if (media && !media.paused) {
                    media.pause();
                    media.currentTime = 0;
                }
            },
            setMuted: function(muted) {
                isMuted = muted;
                media.muted = muted;
            },
            setVolume: function(volume) {
                curVolume = volume;
                media.volume = volume;
            }
        };
        return that;
    }

    function selectNext(tabs, class_) {
        var cur = document.querySelector(tabs + " ." + class_);
        var ne = cur.nextElementSibling;
        if (!ne) {
            ne = cur.parentElement.firstElementChild;
        }
        ne.classList.add(class_);
        cur.classList.remove(class_);
    }

    function selectPrior(tabs, class_) {
        var cur = document.querySelector(tabs + " ." + class_);
        var ne = cur.previousElementSibling;
        if (!ne) {
            ne = cur.parentElement.lastElementChild;
        }
        ne.classList.add(class_);
        cur.classList.remove(class_);
    }

    function stop() {
        var cur, model;
        while (true) {
            cur = document.querySelector("#soundcontrol .running");
            if (!cur) {
                return;
            }
            cur.classList.remove("running");
            alarm.stop();
        }
    }

    function run() {
        var cur, model;
        stop();
        cur = document.querySelector("#soundcontrol .active");
        if (!cur) {
            return;
        }
        cur.classList.add("running");
        alarm.play(cur.id);
    }


    listen(window, "load", function(evt) {
        listen("#next", "click", function() {
            selectNext("#soundcontrol", "active");
        });
        listen("#prior", "click", function() {
            selectPrior("#soundcontrol", "active");
        });
        listen("#run", "click", run);
        listen("#stop", "click", stop);
        alarm = Alarm();
        listen("#volume", "change",function(){
            console.log(this + ".value="+this.value);
            alarm.setVolume(this.value);
        });
        listen("#mute", "click", function(){
            alarm.setMuted(this.checked);
        });
    });

    listen(window, "unload", function(evt) {
        console.log("Hey, the window is unloaded: " + evt);
    });
})();
