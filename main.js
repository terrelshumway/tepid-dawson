function listen(target, event, callback) {
    "use strict";
    if (typeof target === 'string') {
        target = document.querySelector(target);
    }
    target.addEventListener(event, callback);

    return target;
}

var alarm = (function() {
    "use strict";
    var alarm;
    var that = {
        play: function() {
            alarm.play();
        },
        reset: function() {
            alarm.pause();
            alarm.currentTime = 0;
        }
    };

    window.addEventListener("load", function init() {
        alarm = document.getElementById("alarm");
        listen("#mute", "click", function() {
            alarm.muted = this.checked;
        }).checked = alarm.muted;
        listen("#volume", "change", function() {
            alarm.volume = this.value;
        }).value = alarm.volume;
    });
    return that;
})();



/**
TODO: add pause/resume functionality (play button when running)
TODO: add restart functionality
TODO: reconsider what the skip button does
TODO: focus currently selected timer
TODO: rework visibility of controls so ranges are not shown when progress is shown.
TODO: use a promise instead of a callback on expiration
TODO: make sure it only gets called/resolved once
TODO: switch interval to 1 minute when page is not active
TODO: add text field for activity descriptions
TODO: store/sync activity log
TODO: add more timers (e.g. long break after 4 pomoj, lunch after 8)
*/
(function() {
    "use strict";
    var timers = [];
    var current;
    var SCALE_FACTOR = 600;

    function timestr(millis) {
        var neg, mins, secs;
        neg = millis < 0;
        secs = Math.round(Math.abs(millis) / 1000);
        if (secs > 60) {
            mins = Math.floor(secs / 60);
            secs = secs - 60 * mins;
        } else {
            mins = "0";
        }
        return (neg ? "-" : "") + mins + (secs < 10 ? ":0" : ":") + secs;
    }



    function timer(spec) {
        var droot, range, rangetext, progress, progresstext;
        var that, started, paused, max, current, interval, expires;
        var finish;

        function tick() {
            var remaining, now;
            if (expires) {
                now = Date.now();
                remaining = expires - now;
                if (remaining <= 0) {
                    if (typeof finish === 'function') {
                        finish(now);
                        finish = undefined;
                    }
                    progress.value = 0;
                    //that.reset();
                } else {
                    progress.value = remaining;
                }
                progresstext.value = timestr(remaining);
            } else {
                alert("unreachable code");
            }

        }

        function update() {
            max = range.value;
            rangetext.value = max;
            progress.max = max;
            progress.value = max;
        }

        that = {
            /* bind to DOM
             */
            bind: function(domsel) { //ri,ro,pi,po){
                droot = document.querySelector(domsel);
                range = document.querySelector(domsel + "time");
                rangetext = document.querySelector(domsel + "out");
                progress = document.querySelector(domsel + "progress");
                progresstext = document.querySelector(domsel + "pout");
                range.addEventListener("input", update);
                update();
                return that;
            },
            isrunning: function() {
                return !!expires;
            },
            select: function(active) {
                if (active) {
                    droot.classList.add("active");
                } else {
                    droot.classList.remove("active");
                }
            },
            reset: function() {
                if (interval) {
                    clearInterval(interval);
                }
                interval = undefined;
                expires = undefined;
                droot.classList.remove("running");
            },
            start: function(then) {
                var remaining, now = Date.now();
                finish = then;
                started = now;
                remaining = max * SCALE_FACTOR;
                expires = now + remaining;
                interval = window.setInterval(tick, 1000);
                progress.max = remaining;
                progress.value = remaining;
                progresstext.value = timestr(remaining);
                droot.classList.add("running");
            }
        };
        return that;

    }

    function set_current(index) {
        var oldt, newt;
        if (index >= timers.length) {
            index = 0;
        }
        if (index === current) {
            return;
        }
        stop(current);
        timers[current].select(false);
        current = index;
        timers[current].select(true);
    }

    function stop(index) {
        if (typeof index === 'undefined') {
            index = current;
        }
        var oldt = timers[index];
        if (oldt) {
            alarm.reset();
            oldt.reset();
        }
    }

    function play() {
        console.log("play");
        timers[current].start(alarm.play);
    }

    function skip() {
        console.log("skip");
        set_current(current + 1);
    }

    window.addEventListener("load", function() {
        var t, run, stop;
        t = timer();
        t.bind("#work");
        timers.push(t);

        t = timer();
        t.bind("#break");
        timers.push(t);
        set_current(0);
        listen("#run", "click", play);
        listen("#stop", "click", stop);
        listen("#next", "click", skip);

        console.log("loaded");
    });

})();
