The immediate goal of this project is to build a pomodoro.

  * display a 25 minute countdown timer
  * display a 5 minute countdown timer
  * each timer has start/pause/reset buttons

future features:

 * add a dream catcher
 * attach a diary to the pomodoro cycles.
